Summary: Config to consume the repositories for the ISA SIG
Name: centos-release-isa
Version: 1
Release: 1%{?dist}
License: GPLv2
URL: https://sigs.centos.org/isa/
Source0: RPM-GPG-KEY-CentOS-SIG-ISA
Source1: CentOS-ISA-Baseline.repo
Source2: CentOS-ISA-Optimized.repo

BuildArch: noarch

%description
yum configs for repositories as delivered by CentOS ISA SIG.

%package baseline
Summary: Config to consume the baseline repositories for the ISA SIG
Requires: centos-release
Conflicts: %{name}-optimized

%description baseline
ISA Baseline packages were compiled with gcc-12.
Regular CentOS Stream 9 was compiled with gcc-11.
This package contains the dnf configs and post install scripts
to setup your machine to use these recompiled packages.

After you install this package you should run
  dnf distro-sync -y
  reboot

%package optimized
Summary: Config to consume the optimized repositories for the ISA SIG
Requires: centos-release
Conflicts: %{name}-baseline

%description optimized
SA Baseline packages were compiled with gcc-12, but they also had x86_64-v3 enabled.
Regular CentOS Stream 9 was compiled with gcc-11 and x86_64-v2.
This package contains the dnf configs and post install scripts
to setup your machine to use these recompiled packages.

After you install this package you should run
  dnf distro-sync -y
  reboot

%prep
%setup -q -n %{name} -T -c

%install
# gpg key
install -p -d %{buildroot}%{_sysconfdir}/pki/rpm-gpg
install -m 644 %{SOURCE0} %{buildroot}%{_sysconfdir}/pki/rpm-gpg
# openvswitch repo
install -D -m 644 %{SOURCE1} %{buildroot}%{_sysconfdir}/yum.repos.d/CentOS-ISA-Baseline.repo
install -D -m 644 %{SOURCE2} %{buildroot}%{_sysconfdir}/yum.repos.d/CentOS-ISA-Optimized.repo

%posttrans baseline
if grep -q includepkgs /etc/yum.repos.d/centos.repo ; then
    sed -i "s/includepkgs=.*/includepkgs=fdo* gcc-toolset* java-1.8.0-openjdk* keylime-agent-rust libreoffice* pg* postgresql*/" /etc/yum.repos.d/centos.repo
else
    sed -i "s/\[appstream\]/[appstream]\nincludepkgs=fdo* gcc-toolset* java-1.8.0-openjdk* keylime-agent-rust libreoffice* pg* postgresql*/" /etc/yum.repos.d/centos.repo
fi
dnf module disable maven nginx nodejs php postgresql ruby -y >> /dev/null 2>&1
dnf config-manager --set-disabled baseos
dnf config-manager --set-disabled crb
dnf config-manager --set-disabled extras-common
echo
echo "=========="
echo "TO CONVERT THIS MACHINE SO ALL PACKAGES ARE FROM THE ISA REPO:"
echo "    dnf distro-sync -y"
echo "    reboot"
echo "=========="
echo

%posttrans optimized
if grep -q includepkgs /etc/yum.repos.d/centos.repo ; then
    sed -i "s/includepkgs=.*/includepkgs=fdo* gcc-toolset* java-1.8.0-openjdk* keylime-agent-rust libreoffice* pg* postgresql*/" /etc/yum.repos.d/centos.repo
else
    sed -i "s/\[appstream\]/[appstream]\nincludepkgs=fdo* gcc-toolset* java-1.8.0-openjdk* keylime-agent-rust libreoffice* pg* postgresql*/" /etc/yum.repos.d/centos.repo
fi
dnf module disable maven nginx nodejs php postgresql ruby -y >> /dev/null 2>&1
dnf config-manager --set-disabled baseos
dnf config-manager --set-disabled crb
dnf config-manager --set-disabled extras-common
echo
echo "=========="
echo "TO CONVERT THIS MACHINE SO ALL PACKAGES ARE FROM THE ISA REPO:"
echo "    dnf distro-sync -y"
echo "    reboot"
echo "=========="
echo

%files baseline
%{_sysconfdir}/pki/rpm-gpg/RPM-GPG-KEY-CentOS-SIG-ISA
%config(noreplace) %{_sysconfdir}/yum.repos.d/CentOS-ISA-Baseline.repo

%files optimized
%{_sysconfdir}/pki/rpm-gpg/RPM-GPG-KEY-CentOS-SIG-ISA
%config(noreplace) %{_sysconfdir}/yum.repos.d/CentOS-ISA-Optimized.repo

%changelog
* Mon Jul 03 2023 Troy Dawson <tdawson@redhat.com> - 1.1
- Initial release rpm ISA SIG
